<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
//2次元配列
$nicchokus =  array(
    array('浅野','伊藤'),
    array('宇田','江本'),
    array('中本'),
);

echo $nicchokus[0][0].'<br>';
echo $nicchokus[0][1].'<br>';
echo $nicchokus[1][0].'<br>';
echo $nicchokus[1][1].'<br>';
echo $nicchokus[2][0].'<br>';

$nicchokus[] = array('太田','今野');

echo '<pre>';
var_dump($nicchokus);
echo '</pre>';
?>
    </body>
</html>
