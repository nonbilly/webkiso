<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
echo date('Y/m/d H:i:s', 300000);
echo '<br>';
echo date('Y/m/d H:i:s', 0);
echo '<br>';
echo date('Y/m/d H:i:s');
echo '<br>';

function is_sunday($timestamp = null){
    //現在のタイムスタンプを代入
    if($timestamp === null){
        $timestamp = time();
    }
    echo date('Y/m/d', $timestamp) . 'は';
    $week = date('w', $timestamp);
    if($week == 0){
        echo "日曜日です<br>";
    }else{
        echo "日曜日ではありません<br>";
    }
}
$timestamp = time();
echo $timestamp.'<br>';
is_sunday($timestamp);

$timestamp2 = mktime(0, 0, 0, 6, 18, 2017);
echo $timestamp2.'<br>';
is_sunday($timestamp2);

is_sunday();
/*is_sunday(100000);
is_sunday(200000);
is_sunday(300000);
is_sunday(400000);
is_sunday(500000);
is_sunday(600000);
is_sunday(700000);
is_sunday(800000);
is_sunday(900000);
is_sunday(1000000);
is_sunday(1100000);
is_sunday(1200000);
is_sunday(1300000);*/
?>
    </body>
</html>
