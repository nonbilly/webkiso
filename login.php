<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$users = array(
//         array('username' => 'asano','password' => 'asanoasano'),
//         array('username' => 'ando','password' => 'andoando'),
           'ando' => 'andoando',
           'kawashima' => 'kawashimakawashima',
           'tumoto' => 'tumototumoto',
        );
//include
include_once 'auth.php';

if(isset($_POST['username']) && isset($_POST['password'])&&
        password_check($_POST['username'],$_POST['password'])
    ){
    $_SESSION['login'] = true;
    $_SESSION['username'] = $_POST['username'];
}elseif (isset ($_GET['logout']) && $_GET['logout'] == 'yes') {
    $_SESSION = array();
    session_destroy();
}

if( ! isset($_SESSION['login'])){
    echo 'ログインして下さい';
    echo <<<EOT
    <form method="post" action="" enctype="multipart/form-data">
        ユーザー名:
        <input type="text" name="username">
        パスワード:
        <input type="password" name="password">
        <input type="submit" value="ログイン">
    </form>
EOT;
    
    exit;
}
?>
    </body>
</html>
