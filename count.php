<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$names = ['浅野','伊藤','宇田','江本'];
echo count($names);//4

echo '<pre>';
var_dump($names);
echo '</pre>';
echo '<br>';
//配列の最後に文字列を追加する
$names[] = '太田';
echo count($names);

echo $names[(count($names)-1)];

echo '<br><br>';
//ここまで
$empty = [];
echo count($empty);
echo '<br>';

$test = 100;

if(is_array($test)){
    echo count($test);
}else{
    echo '配列ではありません'.'<br>';
}
echo count($test);
echo '<br>';

$count = (is_array($test))?count($test):'×';
echo $count;
?>
    </body>
</html>
