<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
//変数に整数を代入
$a = 123;
//変数に文字列を代入
$b = '321';

echo '$a の型:'. gettype($a) .'<br>';
echo '$b の型:'. gettype($b) .'<br><br>';

$a = strval($a);
$b = intval($b);

echo '$a の型:'. gettype($a) .'<br>';
echo '$b の型:'. gettype($b) .'<br><br>';

$a = (int) $a;
$b = (string) $b;

echo '$a の型:'. gettype($a) .'<br>';

$c = '22abc100';
$c = (int) $c;

echo '$c の型:'. gettype($c) .'<br>';
echo '$c の中身:'. $c .'<br><br>';
?>
    </body>
</html>
