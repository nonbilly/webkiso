<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$result_even = 0;
$result = 0;
for($i=1; $i<=100; $i++){
    $result += $i;
    
    if($i % 2 == 0){
        $result_even += $i;
    }
}

echo $result.'<br>';
echo $result_even.'<br>';

//pattern2

$result = 0;
for($i=0; $i<=100; $i+=2){
    $result += $i;
}
echo $result.'<br>';
?>
    </body>
</html>
