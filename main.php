<?php
include_once 'login.php';

if(isset($_POST['user_add'])){
    if(! empty($_POST['username']) && ! empty($_POST['password']) ){
        //ユーザー追加処理
        add_users($_POST['username'], $_POST['password']);
        //メッセージ表示用
        $message = 'ユーザーを追加しました。';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?= $_SESSION['username']; ?> さん、ようこそ！
<hr>
<?php

    echo '秘密の情報';
    echo '<br>';
    echo '<a href="logout.php">ログアウト</a>';

?>
<hr>
<?php
if(!empty($message)){
    echo $message.'<br>';
}
?>
    <h2>ここからユーザーを追加できます。</h2>
<form method="post" action="" enctype="multipart/form-data">
        ユーザー名:
        <input type="text" name="username">
        パスワード:
        <input type="password" name="password">
        <input type="submit" value="ユーザー追加" name="user_add">
</form>
    </body>
</html>
