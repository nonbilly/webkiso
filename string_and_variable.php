<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>string_and_variable</title>
    </head>
    <body>
<?php
//変数$nameに文字列を代入します。
$name = 'PHP逆引きレシピ';
//変数を$Nameに文字列を代入します。$nameと$Nameは別の変数です。
$Name = 'CodeIgniter徹底入門';
       
//文字列と変数の値をecho文で表示します。
echo '書籍名: ' . $name . '<br>';
echo "書籍名: {$Name}\n<br>";
echo '書籍名: {$Name}\n<br>';
       
$format = '書籍名: %s、%s<br>';
echo sprintf($format, $name, $Name);
       
//複数行の文字列 * EOL; END;のインデントをなくす。
$book = 'PHP逆引きレシピ';
$text = <<<EOL
ヒアドキュメント<br>
書籍名: $book<br>
EOL;
       
echo $text;
echo <<<END
echoも
できます<br><br>
END;
               
echo <<<'END'
Nowdoc構文です。<br>
変数は展開されません<br>
書籍名: $text<br>
END;
?>
    </body>
</html>