<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$week = date('w');
switch ($week){
    case '1':
        echo '月曜日です<br><br>';
        break;
    case '2':
        echo '火曜日です<br><br>';
        break;
    case '3':
        echo '水曜日です<br><br>';
        break;
    case '4':
        echo '木曜日です<br><br>';
        break;
    case '5':
        echo '金曜日です<br><br>';
        break;
    default :
        echo 'おやすみです<br><br>';
        break;
}

if($week == 1){
    echo '月曜日です';
}else if($week == 2){
    echo '火曜日です';
}else if($week == 3){
    echo '水曜日です';
}else if($week == 4){
    echo '木曜日です';
}else if($week == 5){
    echo '金曜日です';
}else{
    echo 'おやすみです';
}
?>
    </body>
</html>
