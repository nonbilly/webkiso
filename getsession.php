<?php
//セッションのセット
session_set_cookie_params(3600);
session_start();
//GETデータがセットされていた場合、
//その内容を保存する（セッション）
if(isset($_GET['name'])){
    $_SESSION['name'] = $_GET['name'];
}
//画面に$session['name']を表示
echo $_SESSION['name'];