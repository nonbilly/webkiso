<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$people = array(
    array('name' => '川島',
          'height' => '175'),
    array('name' => '津本',
          'height' => '180'),
    array('name' => '高嶺',
          'height' => '160'),
    array('name' => '伊与田',
          'height' => '165'),
    );

/*    echo '<table>';
    echo '<thead>';
    echo '<tr>';
    echo '<td>名前</td>';
    echo '<td>身長</td>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';
    foreach ($people as $peoples){
        echo '<tr>';
        echo '<td>' . $people'['name'];
        echo '<td>' . $people'['height'];
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
*/   
?>
        <table>
            <thead>
            <tr>
                <th>名前</th>
                <th>身長</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($people as $person): ?>
                <tr>
                    <th><?= $person['name']; ?></th>
                    <th><?= $person['height']; ?></th>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </body>
</html>
