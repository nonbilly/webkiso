<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php

function get_next_year(){
    $now_year = date('Y');
    $next_year = $now_year+1;
    return $next_year;
} 

$birth_year = 1998;
$age = get_next_year() - $birth_year;
echo "私は{$birth_year}年生まれです。来年は". $age ."歳です。<br>";
?>
    </body>
</html>
