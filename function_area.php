<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
function convert_to_tsubo($area = 100)
{
    $tsubo = $area * 0.3025;
    return $tsubo;
}

$area = 1000;
echo '$areaの初期値は' . $area . 'です。<br>';
$result = convert_to_tsubo($area);
echo '関数に渡された$areaの値は' . $area . 'です。<br>';
echo '関数の実行結果は' . $result . 'です。';
?>
    </body>
</html>
