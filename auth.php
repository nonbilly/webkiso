<?php
function get_users(){
    $data = file_get_contents('users.json');
    if($data===false){
        echo 'データが読み込めませんでした。';
        return array();
    }  else {
        $array = json_decode($data,true);
        return $array;
    }
}

function add_users($name,$pass){
//ユーザーリスト
$users = get_users();
$users[] = array('id'=>$name,'pass'=>$pass);

$json = json_encode($users,JSON_PRETTY_PRINT);
file_put_contents('users.json', $json);
}

function password_check($name,$pass){
//ユーザーリスト
$users = get_users();
    foreach ($users as $user){
        if($user['id']==$name&&
           $user['pass']==$pass){
           //ユーザー名、パスワードが合っていた
           return true;
           }
    }
    //合っているものがない
    return false;
}
/*
echo (password_check('twice', 'once9once')) ? 'OK':'NG';
echo '<br>';
echo (password_check('twice', 'once0once')) ? 'OK':'NG';
*/