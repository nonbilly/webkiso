<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
function is_multiple3($value){
    //$valueが３の倍数かを判定
    //3で割った余りをとる
    $amari = $value % 3;
    if($amari == 0){
        //3の倍数です
        echo "{$value}は3の倍数です<br>";
    }else{
        //3の倍数じゃない
        echo "{$value}は3の倍数ではありません<br>";
    }
}

$value = 2;
is_multiple3($value);
$value = 3;
is_multiple3($value);
$value = 4;
is_multiple3($value);

is_multiple3(10);
is_multiple3(11);
is_multiple3(12);
?>
    </body>
</html>
