<?php
session_start();

//セッションをクリアする
$_SESSION = array();
session_destroy();

//自動でページを遷移する
header('location:main.php');