<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
//if文の条件式に$value(引数)を使って
function is_seinen($value){
    if($value < 20){
        echo "未成年です<br>";
    } elseif ($value == 20) {
        echo "成年しました<br>";
    } else {
        echo "成年済みです<br>";
    }
}

$value = 18;
is_seinen($value);
$value = 19;
is_seinen($value);
$value = 20;
is_seinen($value);
$value = 21;
is_seinen($value);
$value = 22;
is_seinen($value);
?>
    </body>
</html>
