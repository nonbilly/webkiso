<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$names = array('浅野','伊藤','宇田','江本');
echo count($names).'<br>';

$names['oota'] = '太田';
echo count($names).'<br>';

echo $names[0].'<br>';
echo $names[3].'<br>';
echo $names[4].'<br>';
echo $names['oota'].'<br>';

var_dump($names);
?>
    </body>
</html>
